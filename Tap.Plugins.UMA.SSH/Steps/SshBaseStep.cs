﻿// Author:      Bruno Garcia Garcia <bgarcia@lcc.uma.es>
// Copyright:   Copyright 2019-2020 Universidad de Málaga (University of Málaga), Spain


using OpenTap;

namespace Tap.Plugins.UMA.SshInstrument.Steps
{
    public abstract class SshBaseStep : TestStep
    {
        #region Settings
        
        [Display("Instrument", Group: "Instrument", Order: 0.0)]
        public Instruments.SshInstrument Instrument { get; set; }

        #endregion

        public SshBaseStep() { }
    }
}
