﻿// Author:      Bruno Garcia Garcia <bgarcia@lcc.uma.es>
// Copyright:   Copyright 2019-2020 Universidad de Málaga (University of Málaga), Spain


using Renci.SshNet;
using System;

namespace Tap.Plugins.UMA.SshInstrument.Instruments
{
    public class BackgroundSshCommand
    {
        public IAsyncResult AsyncResult { get; set; }
        public SshCommand Command { get; set; }
    }
}
